1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="cs.mad.flashcards"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="24"
8-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="30" />
9-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml
10
11    <application
11-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:5:5-22:19
12        android:allowBackup="true"
12-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:6:9-35
13        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
13-->[androidx.core:core:1.3.2] /Users/alexanderhamilton/.gradle/caches/transforms-2/files-2.1/3332427177a0c51858f805d368bea6a9/core-1.3.2/AndroidManifest.xml:24:18-86
14        android:debuggable="true"
15        android:extractNativeLibs="false"
16        android:icon="@mipmap/ic_launcher"
16-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:7:9-43
17        android:label="@string/app_name"
17-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:8:9-41
18        android:roundIcon="@mipmap/ic_launcher_round"
18-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:9:9-54
19        android:supportsRtl="true"
19-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:10:9-35
20        android:testOnly="true"
21        android:theme="@style/Theme.Flashcards" >
21-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:11:9-48
22        <activity
22-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:12:9-14:40
23            android:name="cs.mad.flashcards.adapters.FlashcardSetDetailActivity"
23-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:13:13-64
24            android:exported="false" />
24-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:14:13-37
25        <activity android:name="cs.mad.flashcards.activities.MainActivity" >
25-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:15:9-21:20
25-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:15:19-58
26            <intent-filter>
26-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:16:13-20:29
27                <action android:name="android.intent.action.MAIN" />
27-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:17:17-69
27-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:17:25-66
28
29                <category android:name="android.intent.category.LAUNCHER" />
29-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:19:17-77
29-->/Users/alexanderhamilton/Downloads/lab3-skeleton-android 2/app/src/main/AndroidManifest.xml:19:27-74
30            </intent-filter>
31        </activity>
32    </application>
33
34</manifest>
