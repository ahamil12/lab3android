package cs.mad.flashcards.adapters
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.MainActivity
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet


class Adapter(private val input: List<FlashcardSet>): RecyclerView.Adapter<Adapter.ViewHolder>() {

    private var myData = input.toMutableList()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewGrid = view.findViewById<TextView>(R.id.my_text)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(View.inflate(parent.context, R.layout.item_grid, null))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textViewGrid.text = myData[position].title
        holder.itemView.setOnClickListener{
            holder.itemView.context.startActivity(Intent(holder.itemView.context, FlashcardSetDetailActivity::class.java))
        }
    }


    override fun getItemCount(): Int {
        return myData.size
    }
}
