package cs.mad.flashcards.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard

class AdapterLinear(input: List<Flashcard>): RecyclerView.Adapter<AdapterLinear.ViewHolder>() {

    private val myData = input.toMutableList()


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
       //val imageView: ImageView = view.view_card
        var textView = view.findViewById<TextView>(R.id.my_textLinear)
        var textView2 = view.findViewById<TextView>(R.id.my_textLinear2)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterLinear.ViewHolder {
        return ViewHolder(View.inflate(parent.context, R.layout.item_linear, null))
    }

    override fun getItemCount(): Int {
        return myData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView2.text = myData[position].term
        holder.textView.text = myData[position].definition
    }

}




