package cs.mad.flashcards.activities
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.Adapter
import cs.mad.flashcards.adapters.AdapterLinear
import cs.mad.flashcards.entities.FlashcardSet


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerGrid = findViewById<RecyclerView>(R.id.recyclerGrid)
        recyclerGrid.adapter = Adapter(FlashcardSet.getHardcodedFlashcardSets())

    }
}

